package com.me_java.components;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

public class JTextFielldAndHandler extends JFrame {
	private static final long serialVersionUID = 1L;

	private JTextField tf = new JTextField();
	// row, column
	private JTextArea txtArea = new JTextArea(5, 20);

	public JTextFielldAndHandler() {
		setTitle("JTextField");
		setLocation(500, 200);
		setSize(250, 100);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addHandler();

		setVisible(true);
	}

	public void addHandler() {

		tf.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				System.out.println(e.getKeyCode());
				// 10 = enter
				// 32 = space
				if (e.getKeyCode() < 91 && e.getKeyCode() >= 65 || (e.getKeyCode() == 32 || e.getKeyCode() == 10))
					txtArea.setText(txtArea.getText() + e.getKeyChar());
			}
		});

		JPanel panel = new JPanel(new MigLayout("CENTER", "", "[]20[]"));
		panel.add(tf, "width 200::200, wrap");
		panel.add(txtArea);

		add(panel);
	}

	public static void main(String[] args) {
		new JTextFielldAndHandler();
	}

}
