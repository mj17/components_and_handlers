package com.me_java.components;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class JCheckBoxAndHandler extends JFrame {
	private static final long serialVersionUID = 1L;

	private JCheckBox ckA = new JCheckBox("A");
	private JCheckBox ckB = new JCheckBox("B");
	private JCheckBox ckAll = new JCheckBox("All");
	private int count = 0;

	public JCheckBoxAndHandler() {
		setTitle("JCheckBox");
		setLocation(500, 200);
		setSize(250, 100);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addHandler();

		setVisible(true);
	}

	public void addHandler() {
		ItemListener listener = new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox source = (JCheckBox) e.getSource();
				if (!source.getText().equals("All")) {
					if (e.getStateChange() == ItemEvent.DESELECTED) {
						count--;
						ckAll.setSelected(false);
					} else {
						count++;
						if (count == 2)
							ckAll.setSelected(true);
					}
				} else if (source.getText().equals("All")) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						ckA.setSelected(true);
						ckB.setSelected(true);
						ckAll.setSelected(true);
					}
				}
			}
		};
		ckA.addItemListener(listener);
		ckB.addItemListener(listener);
		ckAll.addItemListener(listener);

		JPanel panel = new JPanel(new FlowLayout(10, 20, 20));
		panel.add(ckA);
		panel.add(ckB);
		panel.add(ckAll);

		add(panel);
	}

	public static void main(String[] args) {
		new JCheckBoxAndHandler();
	}

}
