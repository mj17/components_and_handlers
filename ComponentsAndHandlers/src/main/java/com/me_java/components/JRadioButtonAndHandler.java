package com.me_java.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import net.miginfocom.swing.MigLayout;

public class JRadioButtonAndHandler extends JFrame {
	private static final long serialVersionUID = 1L;

	private JRadioButton rbShow = new JRadioButton("Show");
	private JRadioButton rbHide = new JRadioButton("Hide");
	private ButtonGroup gp = new ButtonGroup();
	private JLabel lbName = new JLabel("");

	public JRadioButtonAndHandler() {
		setTitle("JRadioButton");
		setLocation(500, 200);
		setSize(250, 100);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addHandler();

		setVisible(true);
	}

	public void addHandler() {
		gp.add(rbShow);
		gp.add(rbHide);
		rbHide.setSelected(true);

		rbHide.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lbName.setText("");
			}
		});
		rbShow.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (((JRadioButton) e.getItem()).getText().equals("Show"))
					lbName.setText("ME & Java");
			}
		});

		JPanel panel = new JPanel(new MigLayout("CENTER", "", "[]20[]"));
		panel.add(rbShow);
		panel.add(rbHide, "wrap");
		panel.add(lbName, "spanx 2, gapx 25");

		add(panel);

	}

	public static void main(String[] args) {
		new JRadioButtonAndHandler();
	}

}
