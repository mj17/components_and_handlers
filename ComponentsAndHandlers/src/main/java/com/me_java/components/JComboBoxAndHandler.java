package com.me_java.components;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class JComboBoxAndHandler extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JComboBox<String> comboBox;
	private String[] fruitArray = { "Apple", "Orange", "Strawberry", "Pineapple"};
	private JLabel showLable = new JLabel();

	public JComboBoxAndHandler() {
		setTitle("JComboBox");
		setLocation(500, 200);
		setSize(250, 100);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addHandler();

		setVisible(true);
	}

	public void addHandler() {
		showLable.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		comboBox = new JComboBox<String>(fruitArray);
		
		comboBox.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String str = (String) comboBox.getSelectedItem();
				showLable.setText(str);
			}
		});

		JPanel panel = new JPanel(new FlowLayout(10, 20, 20));
		panel.add(comboBox);
		panel.add(showLable);
		add(panel);
	}

	public static void main(String[] args) {
		new JComboBoxAndHandler();
	}

}
